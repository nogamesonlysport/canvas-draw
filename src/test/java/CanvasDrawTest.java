import junit.framework.TestCase;
import org.junit.Test;

/**
 * Created by amitsjoshi on 10/04/18.
 */
public class CanvasDrawTest extends TestCase
{
    @Test
    public void testCanvasCreation()
    {
        CanvasDraw canvasDraw = new CanvasDraw();
        int w = 20;
        int h = 4;
        canvasDraw.createCanvas(20, 4);

        char [][] canvas = canvasDraw.getCanvas();

        for(int i=0; i<(h+2); i++)
        {
            for(int j=0; j<(w+2); j++)
            {
                if(i == 0 || i == (w-1))
                {
                    assertEquals('-', canvas[i][j]);
                }
                if((j == 0 || j == (w+1)) && (i == 1 || i == 2 || i == 3 || i == 4))
                {
                    assertEquals('|', canvas[i][j]);
                }
            }
        }
    }

    @Test
    public void testLineDraw()
    {
        CanvasDraw canvasDraw = new CanvasDraw();
        int w = 20;
        int h = 4;
        canvasDraw.createCanvas(20, 4);
        canvasDraw.drawLine(2, 2, 10, 2);

        char [][] canvas = canvasDraw.getCanvas();

        for(int i=0; i<(h+2); i++)
        {
            for(int j=0; j<(w+2); j++)
            {
                if(i == 0 || i == (w-1))
                {
                    assertEquals('-', canvas[i][j]);
                }
                if((j == 0 || j == (w+1)) && (i == 1 || i == 2 || i == 3 || i == 4))
                {
                    assertEquals('|', canvas[i][j]);
                }
            }
        }

        for(int i=2; i<=10; i++)
        {
            assertEquals('x', canvas[2][i]);
        }
    }

    @Test
    public void testDiagonalLineDraw()
    {
        CanvasDraw canvasDraw = new CanvasDraw();
        int w = 20;
        int h = 4;
        canvasDraw.createCanvas(20, 4);
        canvasDraw.drawLine(2, 2, 10, 4);

        char [][] canvas = canvasDraw.getCanvas();

        for(int i=0; i<(h+2); i++)
        {
            for(int j=0; j<(w+2); j++)
            {
                if(i == 0 || i == (w-1))
                {
                    assertEquals('-', canvas[i][j]);
                }
                if((j == 0 || j == (w+1)) && (i == 1 || i == 2 || i == 3 || i == 4))
                {
                    assertEquals('|', canvas[i][j]);
                }
            }
        }

        assertEquals('x', canvas[2][2]);
        assertEquals('x', canvas[3][6]);
        assertEquals('x', canvas[4][10]);
    }

    @Test
    public void testRectangleDraw()
    {
        CanvasDraw canvasDraw = new CanvasDraw();
        int w = 20;
        int h = 4;
        canvasDraw.createCanvas(20, 4);
        canvasDraw.drawRectangle(2, 2, 6, 4);

        char [][] canvas = canvasDraw.getCanvas();

        for(int i=0; i<(h+2); i++)
        {
            for(int j=0; j<(w+2); j++)
            {
                if(i == 0 || i == (w-1))
                {
                    assertEquals('-', canvas[i][j]);
                }
                if((j == 0 || j == (w+1)) && (i == 1 || i == 2 || i == 3 || i == 4))
                {
                    assertEquals('|', canvas[i][j]);
                }
            }
        }

        //testing the 4 edges of the rectangle
        for(int i=2; i<=6; i++)
        {
            assertEquals('x', canvas[2][i]);
        }
        for(int i=2; i<=6; i++)
        {
            assertEquals('x', canvas[4][i]);
        }
        for(int j=2; j<=4; j++)
        {
            assertEquals('x', canvas[j][2]);
        }
        for(int j=2; j<=4; j++)
        {
            assertEquals('x', canvas[j][6]);
        }
    }

    @Test
    public void testBucketFill()
    {
        CanvasDraw canvasDraw = new CanvasDraw();
        int w = 20;
        int h = 4;
        canvasDraw.createCanvas(20, 4);
        canvasDraw.bucketFill(10, 2, 'o');

        char [][] canvas = canvasDraw.getCanvas();

        for(int i=0; i<(h+2); i++)
        {
            for(int j=0; j<(w+2); j++)
            {
                if(i == 0 || i == (w-1))
                {
                    assertEquals('-', canvas[i][j]);
                }
                if((j == 0 || j == (w+1)) && (i == 1 || i == 2 || i == 3 || i == 4))
                {
                    assertEquals('|', canvas[i][j]);
                }
            }
        }

        for(int i=1; i<=20; i++)
        {
            for(int j=1; j<=4; j++)
            {
                assertEquals('o', canvas[j][i]);
            }
        }
    }

    @Test
    public void testBucketFillWithRectangle()
    {
        CanvasDraw canvasDraw = new CanvasDraw();
        int w = 20;
        int h = 4;
        canvasDraw.createCanvas(20, 4);
        canvasDraw.drawRectangle(2, 2, 6, 4);
        canvasDraw.bucketFill(10, 2, 'o');

        char [][] canvas = canvasDraw.getCanvas();

        for(int i=0; i<(h+2); i++)
        {
            for(int j=0; j<(w+2); j++)
            {
                if(i == 0 || i == (w-1))
                {
                    assertEquals('-', canvas[i][j]);
                }
                if((j == 0 || j == (w+1)) && (i == 1 || i == 2 || i == 3 || i == 4))
                {
                    assertEquals('|', canvas[i][j]);
                }
            }
        }

        //testing the 4 edges of the rectangle
        for(int i=2; i<=6; i++)
        {
            assertEquals('x', canvas[2][i]);
        }
        for(int i=2; i<=6; i++)
        {
            assertEquals('x', canvas[4][i]);
        }
        for(int j=2; j<=4; j++)
        {
            assertEquals('x', canvas[j][2]);
        }
        for(int j=2; j<=4; j++)
        {
            assertEquals('x', canvas[j][6]);
        }

        //test the rectangle area doesn't get painted by bucketFill
        for(int i=3; i<=5; i++)
        {
            assertEquals(' ', canvas[3][i]);
        }

        //the entire canvas apart from the rectangle has been painted with 'o'
        for(int i=1; i<=20; i++)
        {
            for(int j=1; j<=4; j++)
            {
                if(!(2 <= i && i <=6 && 2 <= j && j <= 4))
                {
                    assertEquals('o', canvas[j][i]);
                }
            }
        }
    }
}
