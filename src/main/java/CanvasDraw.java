import java.util.Scanner;

/**
 * Created by amitsjoshi on 09/04/18.
 */
public class CanvasDraw
{
    private char [][] canvas;
    private int w;
    private int h;

    public static void main(String[] args)
    {
        CanvasDraw console = new CanvasDraw();

        Scanner scanner = new Scanner(System.in);
        char command = 'C';
        do{
            System.out.print("enter command: ");

            command = scanner.next().charAt(0);

            switch(command)
            {
                case 'C':
                    console.createCanvas(scanner.nextInt(), scanner.nextInt());
                    break;
                case 'L':
                    console.drawLine(scanner.nextInt(), scanner.nextInt(), scanner.nextInt(), scanner.nextInt());
                    break;
                case 'R':
                    console.drawRectangle(scanner.nextInt(), scanner.nextInt(), scanner.nextInt(), scanner.nextInt());
                    break;
                case 'F':
                    console.quadrilateral(scanner.nextInt(), scanner.nextInt(), scanner.nextInt(), scanner.nextInt(),
                            scanner.nextInt(), scanner.nextInt(), scanner.nextInt(), scanner.nextInt());
                    break;
                case 'B':
                    console.bucketFill(scanner.nextInt(), scanner.nextInt(), scanner.next().charAt(0));
                    break;
            }
            console.paintCanvas();
        }while(command != 'Q');
    }

    private void quadrilateral(int x1, int y1, int x2, int y2, int x3, int y3, int x4, int y4)
    {
        drawLine(x1, y1, x2, y2);
        drawLine(x2, y2, x3, y3);
        drawLine(x3, y3, x4, y4);
        drawLine(x4, y4, x1, y1);
    }

    protected void bucketFill(int x, int y, final char o)
    {
        bucketFillAlgo(x, y, o, false);
    }

    private void bucketFillAlgo(int x, int y, final char o, boolean block)
    {
        if(x <= 0 || x >= (w-1) || y < 1 || y >= (h-1))
        {
            return;
        }

        if(canvas[y][x] == 'x')
        {
            block = true;
            return;
        }

        if(canvas[y][x] == o)
        {
            return;
        }

        canvas[y][x] = o;

        if(!block)
        {
            bucketFillAlgo(x+1, y, o, block);
            bucketFillAlgo(x-1, y, o, block);
            bucketFillAlgo(x, y+1, o, block);
            bucketFillAlgo(x, y-1, o, block);
        }
    }

    protected void drawRectangle(final int x1, final int y1, final int x2, final int y2)
    {
        drawLine(x1, y1, x2, y1);
        drawLine(x2, y1, x2, y2);
        drawLine(x1, y2, x2, y2);
        drawLine(x1, y1, x1, y2);
    }

    protected void drawLine(final int x1, final int y1, final int x2, final int y2)
    {
        if(y1 == y2)
        {
            int xMin = 0;
            int xMax = 0;
            if(x1 < x2)
            {
                xMin = x1;
                xMax = x2;
            }
            else
            {
                xMin = x2;
                xMax = x1;
            }
            for(int i=xMin; i<=xMax; i++)
            {
                canvas[y1][i] = 'x';
            }
        }
        else if(x1 == x2)
        {
            int yMin = 0;
            int yMax = 0;
            if(y1 < y2)
            {
                yMin = y1;
                yMax = y2;
            }
            else
            {
                yMin = y2;
                yMax = y1;
            }
            for(int i=yMin; i<=yMax; i++)
            {
                canvas[i][x1] = 'x';
            }
        }
        else
        {
            //draw a diagonal line
            double slope = ((double)(y2 - y1) /(x2 - x1));
            double constant = y2 - (slope * x2);

            int minY = 0;
            int maxY = 0;
            if(y1 < y2)
            {
                minY = y1;
                maxY = y2;
            }
            else
            {
                minY = y2;
                maxY = y1;
            }

            for(int y = (minY + 1); y < maxY; y++)
            {
                int x = (int) ((y - constant)/slope);
                canvas[y][x] = 'x';
            }
            canvas[y1][x1] = 'x';
            canvas[y2][x2] = 'x';
        }
    }

    private void paintCanvas()
    {
        for(int i=0; i<h; i++)
        {
            for(int j=0; j<w; j++)
            {
                System.out.print(canvas[i][j]);
            }
            System.out.print("\n");
        }
    }

    protected void createCanvas(final int width, final int height)
    {
        w = width + 2;
        h = height + 2;

        canvas = new char[h][w];

        for(int r = 0; r < h; r++)
        {
            for(int c = 0; c < w; c++)
            {
                canvas[r][c] = ' ';
                if(c == 0 || c == (w-1))
                {
                    canvas[r][c] = '|';
                }
                if(r==0 || r == (h-1))
                {
                    canvas[r][c] = '-';
                }
            }
        }
        System.out.println("A canvas with width " + width + " and height "
                + height + " has been created with top left coordinate (1,1) " +
                "and bottom right coordinate (20, 4)");
    }

    public char[][] getCanvas()
    {
        return canvas;
    }
}
