Sample output:

enter command: C 20 4
A canvas with width 20 and height 4 has been created with top left coordinate (1,1) and bottom right coordinate (20, 4)
----------------------
|                    |
|                    |
|                    |
|                    |
----------------------
enter command: L 1 2 5 2
----------------------
|                    |
|xxxxx               |
|                    |
|                    |
----------------------
enter command: R 8 2 15 4
----------------------
|                    |
|xxxxx  xxxxxxxx     |
|       x      x     |
|       xxxxxxxx     |
----------------------
enter command: B 18 2 o
----------------------
|oooooooooooooooooooo|
|xxxxxooxxxxxxxxooooo|
|ooooooox      xooooo|
|oooooooxxxxxxxxooooo|
----------------------
enter command: Q
----------------------
|oooooooooooooooooooo|
|xxxxxooxxxxxxxxooooo|
|ooooooox      xooooo|
|oooooooxxxxxxxxooooo|
----------------------

Process finished with exit code 0